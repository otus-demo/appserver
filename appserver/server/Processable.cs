﻿using server;

namespace AppServer.Commands
{
    public class Processable : IProcessable
    {
        IContext _context;

        public Processable(IContext context)
        {
            _context = context;
        }

        public bool CanContinue => (bool)_context.Context["canContinue"];

        public void Process()
        {
            ((Action)_context.Context["process"])();
        }

        public void Terminate(Exception ex)
        {
            ((Action<Exception>)_context.Context["terminate"])(ex);
        }
    }
}
