﻿using server;
using System.Collections.Concurrent;

namespace AppServer.Commands
{
    public class InitProcessorContextCommand : ICommand
    {
        IContext _context;
        public InitProcessorContextCommand(IContext context)
        {
            _context = context;
        }

        public void Execute()
        {
            var queue = new BlockingCollection<ICommand>();
            _context.Add("queue", queue);

            _context.Context["exceptionHandler"] = (ICommand cmd, Exception ex) =>
            {
                throw ex;
            };

            _context.Context["terminate"] = (Exception ex) =>
            {
                _context.Context["canContinue"] = false;
                _context.Add("terminateException", ex);
            };


            _context.Add("canContinue", true);

            _context.Add("process", () =>
            {
                var cmd = queue.Take();
                try
                {
                    cmd.Execute();
                }
                catch (Exception ex)
                {
                    ((Action<ICommand, Exception>)_context.Context["exceptionHandler"])(cmd, ex);
                }
            });
        }
    }
}
