﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server
{
    public class Context : IContext
    {
        private readonly IDictionary<string, object> _context
            = new Dictionary<string, object>();
        IDictionary<string, object> IContext.Context => _context;

        public void Add(string key, object value)
        {
            _context.Add(key, value);
        }
    }
}
