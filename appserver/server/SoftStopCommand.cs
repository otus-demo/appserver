﻿using server;
using System.Collections.Concurrent;

namespace AppServer.Commands
{
    public class SoftStopCommand : ICommand
    {
        IContext _context;
        public SoftStopCommand(IContext context)
        {
            _context = context;
        }

        public void Execute()
        {
            Action previousProcess = (Action)_context.Context["process"];
            _context.Context["process"] = () =>
            {
                previousProcess();
                var queue = (BlockingCollection<ICommand>)_context.Context["queue"];
                if (0 == queue.Count)
                {
                    _context.Context["canContinue"] = false;
                }
            };
        }
    }
}
