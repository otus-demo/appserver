﻿using server;

namespace AppServer.Commands
{
    public class HardStopCommand : ICommand
    {
        IContext _context;
        public HardStopCommand(IContext context)
        {
            _context = context;
        }

        public void Execute()
        {
            _context.Context["canContinue"] = false;
        }
    }
}

