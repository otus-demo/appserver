﻿using server;

namespace AppServer.Commands
{
    public class RegisterProcessorCommand : ICommand
    {
        public void Execute()
        {
            Ioc.Resolve<ICommand>("IoC.Register", "Server.CommandProcessor", (object[] args) =>
            {
                IContext context = new Context();
                new InitProcessorContextCommand(context).Execute();
                context.Add("processor", new Processor(new Processable(context)));
                return context;
            }).Execute();
        }
    }
}
