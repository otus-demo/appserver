﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server
{
    public interface IContext
    {
        public IDictionary<string, object> Context { get; }
        public void Add(string key, object value);
    }
}
